#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#----------------- ALIAS
alias ls='ls --color=auto'
alias grep='grep -n --color'


#----Emilia 
[[ -f ~/.bash_emilia ]] && . ~/.bash_emilia
