#!/bin/bash

#Setup Ext. & shell-user-theme
# gsettings set org.gnome.shell disable-user-extensions false

dconf update

gnome-shell-extension-tool -e user-theme@gnome-shell-extensions.gcampax.github.com
gnome-shell-extension-tool -e alternate-tab@gnome-shell-extensions.gcampax.github.com
gnome-shell-extension-tool -e launch-new-instance@gnome-shell-extensions.gcampax.github.com
gnome-shell-extension-tool -e drive-menu@gnome-shell-extensions.gcampax.github.com

gnome-shell-extension-tool -e dash-to-panel@jderose9.github.com 

#devido aos de cima, aqui era melhor pra lidar com dbus. problema n
gsettings set org.gnome.desktop.interface gtk-theme 'Daisy'
gsettings set org.gnome.desktop.interface cursor-theme 'capitaine-cursors-light'
gsettings set org.gnome.desktop.interface icon-theme 'Papirus'
gsettings set org.gnome.shell disable-user-extensions false 
gsettings set org.gnome.shell favorite-apps "['nautilus.desktop','firefox.desktop']"
gsettings set org.gnome.shell.extensions.user-theme name 'Daisy'
gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/emilia/emilia-timing.xml'
