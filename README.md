# Emilia Config

WARNING, REPOSITORY IS GOING TO BE ARCHIVED FOR THE REALIZATION THAT WITH EMILIA GARDEN IN PLACE, THIS PROJECT IS USELESS

Automatizar coisas simples faz parte da essência de um desenvolvedor e entusiastas Linux. Mas as vezes, é conveniente ter uma forma mais simples de divulgar e organizar seus scripts de configuração.

Com este pensamento, este programa surgiu como uma forma de simplificar as próprias configurações da Emilia, mas pode ser usado com scripts customizáveis.

## Instalação

Todos os programas de Emilia vem com um PKGBUILD para Distribuições do pacman. Mas se deseja montar você mesmo, execute o que está nas funções de build() e package().

## Criação de Configs

Dois arquivos são disponibilizados para servirem de exemplo e são chamados de exemple e exemple-minimal. O exemple contém comentários para auxiliar a criação de cada um dos aspectos.

### Valores e variaveis do arquivo

CREATOR e DESCRIPTION: Nome e descrição. Nada demais.

AUTOSTART_SCRIPT_DAILY e INITIAL: Eles são instalados no autostart do usuário, e são executados no login. DAILY é executado em todos os logins, e INITIAL é executado apenas uma vez, e depois eliminado.

SYSTEMD_ENABLE_LIST e SYSTEMD_DISABLE_LIST: No momento, não implementados ainda, mas eles alterarão valores no systemctl.

DBUS_LIST: Esquemas e Chaves a serem alterados no dbus. Um exemplo de como usar se encontra no arquivo exemple, mas se constitui de um vetor, em que cada linha contém 3 partes: esquema, chave, e valor novo do dbus.

PACMAN_PACKAGES: Lista de pacotes do pacman se necessário. Segue mesma estrutura de um PKGBUILD com dependências.

HAS_RESOURCES: Valor que determina se há uma pasta de mesmo nome com _resources na frente existe junto do config. Caso tenha, a função install_resources será executada.

REQUIRES_ROOT: Determina se este config necessita de root para ser executado. Alguns aspectos como possuir um PACMAN_PACKAGES não vazio pode causar a variável a ir automaticamente para Verdadeiro (1).

main_script: Função que o Config executa caso não se encaixe nas variáveis acima. Qualquer coisa customizável pode ir para lá, mas será executado na hora.

install_resources: Função onde se define o que será feito com os resources do config caso ele tenha. Pulado caso HAS_RESOURCES for 0 ou não existir. Há uma variavel chamada Destiny disponível nesta função que armazena a home do usuário de destino, caso ache conveniente.

## Uso

Os Manuais (man pages) de todos os programas se encontram no pacote emilia-manpages. Porém, o programa também fornece uma ajuda se usado -h.

O programa requer estar instalado, devido a procurar a pasta /usr/share/emilia/config/list, onde estão os arquivos de config do sistema.

Para listar os configs disponíveis, dê o comando ls nesta pasta mencionada e ignore os diretórios. Uma solução melhor está sendo pensada enquanto isso...

```bash
emilia-config -C <config-desejada> 
```

### Opções a mais:
-U <Nome do usuário>: Para usar usuários que não o seu. Requer root. 

-p : pula PACMAN_PACKAGES e não instala os pacotes.

-u : Atualiza o sistema antes. Se não usado pode causar update parcial e Arch Linux não recomenda!

-s : Instala também os arquivos do resource no /etc/skel, vale para resources(caso Destiny seja usado) e para as variáveis do DBus (colocado no arquivo padrão dele no /etc/. Requer root.

## Contribuindo

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue na página para que possa ser discutido.

## Problemas conhecidos

Systemd não possui nada implementado ainda. Não implementamos por não precisar no momento, mas será feito no futuro quando tempo e oportunidades surgirem.

Dbus reclama de não haver um arquivo /etc/dconf/db/local.d/01-Emilia: Só crie este arquivo com o comando touch. Ele vem por padrão na Emilia devido a isso, e caso não tenha mesmo usando Emilia, reporte um bug.


## Licença

Emilia usa GPL3 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[GPL3](https://choosealicense.com/licenses/gpl-3.0/)
